import React from 'react';
import ProductList from './components/product-list/Product-list.js';
import styled, { createGlobalStyle } from 'styled-components';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Favorites from './components/favorites-component/Favorites.js';
import NavComponent from './components/nav-component/NavComponent.js';
import Buy from './components/buy-component/Buy.js';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import thunk from 'redux-thunk';
import { applyMiddleware, compose, createStore } from 'redux';
import persistStore from 'redux-persist/es/persistStore';
import persistedReducer from './store/reducer';

const StoreComponent = styled.div`
  min-width: 1200px;
  margin: 0;
  padding: 0;
  background-color: rgba(81, 44, 91, 1);
`;

const GlobalStyle = createGlobalStyle`
 * {
  box-sizing: border-box;
  margin: 0;
  padding: 0;
  a {
  text-decoration: none;
    }  
    .button {
    font-size: 10px;
    color: rgb(255, 255, 255);
    line-height: 1.8;
    border-radius: 20px;
    background-color: rgb(42, 42, 46);
    cursor: pointer;
    margin: 2px 2px 25px 2px;
  }
 }
`;
const composeEnhancers =
  typeof window === 'object' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({})
    : compose;
const enhancer = composeEnhancers(applyMiddleware(thunk));
const store = createStore(persistedReducer, enhancer);
const persistor = persistStore(store);
function App() {
  return (
    <Provider store = {store}>
      <PersistGate loading ={null} persistor = {persistor}>
       <Router>
         <StoreComponent>
         <GlobalStyle />
         <NavComponent />
         <Routes>
          <Route path='/' exact element={<ProductList />} />
          <Route path='/favorites' element={<Favorites />} />
          <Route path='/buy' element={<Buy />} />
        </Routes>
      </StoreComponent>
    </Router>
    </PersistGate>
    </Provider>
  );
}

export default App;