const deleteFavoritesCard = async (path, idProduct) => {
    const response = await fetch(`http://localhost:7000/${path}/${idProduct}`, {
      method: 'DELETE',
    });
    return response;
  };
  
  export default deleteFavoritesCard;